// This file is a bit of a mish-mash of stuff, not intended to be used in any
// of the apps, but to act as a reference.
utils.test_function();

// Example of a transition with callback
var transition_data_out = {
	'direction_out' : 'up',
	'direction_in' : 'up',
	'target' : '.header',
	'duration' : 600
}

var transition_data_in = {
	'target' : '.header',
	'duration' : 1000
}

// transitions.slide_out(	
// 	transition_data_out,
// 	function() {
// 		transitions.slide_in(transition_data_in)
// 	}
// )

// Another example of a transition with callback
var transition_data_out_content = {
	'direction_out' : 'down',
	'direction_in' : 'down',
	'target' : '#content',
	'duration' : 600
}

var transition_data_in_content = {
	'target' : '#content',
	'duration' : 1000
}

// transitions.slide_out(	
// 	transition_data_out_content,
// 	function() {
// 		transitions.slide_in(transition_data_in_content)
// 	}
// )

// Init Chart
var chart = c3.generate({
    data: {
        columns: [
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50]
        ],
        type: 'spline',
    },
    grid: {
        x: {
            show: true
        },
        y: {
            show: true
        }
    }
});

// Init Daterange Picker
$('#datepicker').daterangepicker({
	'locale': {
        'format': 'DD MMM YYYY HH:mm'
    },
	'opens': 'left',
	'timePicker24Hour' : true,
    'timePicker' : true,
    // 'timePickerIncrement' : 60,
    'startDate' : '01 Jun 2015 00:00',
	'endDate' : '30 Aug 2015 23:59',
	'minDate' : '01 Jun 2015 00:00',
	'maxDate' : '30 Aug 2015 23:59',
    "buttonClasses": "bttn",
    "applyClass": "success_hover",
    "cancelClass": "secondary caution_hover"
}).on('hide.daterangepicker, show.daterangepicker', function() {
	var scrolled = $(window).scrollTop();

	$('.daterangepicker').css({
		'transform' : 'translateY(0px)'
	}).attr('data-start_scroll', scrolled)
});

// This is a quick test for the input[type='range'] input.
// Need to use 'input' event since 'change' will only be fired when you have
// stopped sliding: Not instant feedback.
$('#slider').on('input', function() {
		console.log($(this).val())
});

// Sticky Element stuff...
// Since we're going to be using requestAnimationFrame we need a polyfill for IE9<
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// This should be an Application level variable/object.
var elements = sticky_elements.get_sticky_elements();

// This event should be placed in Application.js, we only want one scroll event to 
// be happening.
var scrolled = 0,
	current_scrolled = 0,
	header_height = 60,
	ticking = false;

$(window).scroll(function() {
	scrolled = $(this).scrollTop()

	// We're going to attempt to smooth the onscroll events out by using
	// requestAnimationFrame as a debouncing mechanism.

	// If 'scrolled' is 0 then we just fire the update_scroll function to reset
	// things nicely.
	// If not, then we fire the function request_tick to see if we're currently awaiting
	// a frame to complete/be drawn.
	if (scrolled === 0) {
		update_scroll()
	} else {
		request_tick()
	}
	

});

function request_tick() {
	// If a frame is still being drawn, 'ticking' will be true, so we won't attempt
	// to do any more stuff.
	if(!ticking) {
		requestAnimationFrame(update_scroll)
		ticking = true
	}	
}

function update_scroll() {
	// This function gets fired by requestAnimationFrame when the browser is
	// theoretically ready to draw another frame.

	// Set ticking to false since we should be ready to set up another requestAnimationFrame
	ticking = false

	// Check if the current_scrolled is different from scrolled (or at 0), then
	// let's finally do some things to the page...
	if (current_scrolled !== scrolled || scrolled === 0) {
		sticky_elements.scroll_sticky_elements(elements,scrolled,header_height)
	}

}

/*
*
*
*
	The following events should happen in the views of the app
*
*
*
*/
// Hide the menus if we click anywhere outside of them.
$('html').click(function() {
		$('.header .container .row [class*="col_"].clickable.clicked').each(function() {
		$(this).removeClass('clicked')
	})
});

$('.header .container .row [class*="col_"].clickable').on('click', function(evt) {
	evt.preventDefault();
	evt.stopPropagation();

	var that = this;
	$('.header .container .row [class*="col_"].clickable.clicked').each(function() {
		if (this != that) {
			$(this).removeClass('clicked')
		}
	})

	// We want to reset the scroll of the dropdown to the top to give
	// some kind of consistency when displaying them.
	$('.dropdown', this).scrollTop(0);

	$(this).toggleClass('clicked')
})

$('.header .container .row [class*="col_"] .dropdown .row.clickable').on('click', function(evt) {
	// In all likelyhood we'll probably want this event to propagate so that
	// the menu is hidden, but this is here for testing.
	evt.stopPropagation();
})

// If a dropdown row has the class .selected, we don't want to propagate
// the click to the parent container (which would close the dropdown)
$('.header .container .row [class*="col_"] .dropdown .row.selected').on('click', function(evt) {
	evt.stopPropagation();
})

// So that they header item for the dropdown doesn't display a permanent
// hover state when on the dropdown, we add a class to it when hovering on
// the dropdown.
$('.header .container .row [class*="col_"] .dropdown').mouseenter(function(evt) {
	$(this).parent().addClass('dropdown_hover')
}).mouseleave(function() {
	$(this).parent().removeClass('dropdown_hover')
}).on('mousewheel DOMMouseScroll', function(evt) {
	// We don't want the document to scroll when we reach the end of the
	// dropdown scroll, so we do this...

		// For Webkit/Chrome and Firefox scroll events.
    var d = evt.originalEvent.wheelDelta || -evt.originalEvent.detail,
    	// If 'd' is greater than 0 then 'dir' equals up, else equals down.
        dir = d > 0 ? 'up' : 'down',
        // If 'dir' is equal to 'up' and scroll is at 0 = true.
        // If 'dir' is equal to 'down' and scroll is at or greater than
        // the height off the element = true.
        stop = (dir == 'up' && this.scrollTop == 0) || (dir == 'down' && this.scrollTop >= this.scrollHeight-this.offsetHeight);
    
    // If stop equals true then prevent the scrolling.
    stop && evt.preventDefault();
})