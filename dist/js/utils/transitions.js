var transitions = {
	
	'distances' :  {
		'out' : 18,
		'in' : 36
	},
	
	'easing' : {
		'standard' : [0.580, 0.000, 0.420, 1.000]
	},

	slide_out: function(data,callback) {
	    var that = this;

	    // If any transitions are currently underway for this element, stop those.
	    $(data.target).velocity('stop')

	    // We want to be able to set a speed when we call the function, but we also want
	    // to have a default that we fall back to if it's not specificed.
	    if (!data.duration && data.duration !== 0) {
	    	data.duration = 350
	    }

	    // We want to be able to set a direction out when we call the function, but we also want
	    // to have a default that we fall back to if it's not specificed.
	    if (!data.direction_out) {
	    	data.direction_out = 'down'
	    }

	    // We want to be able to set a direction in when we call the function, but we also want
	    // to have a default that we fall back to if it's not specificed.
	    if (!data.direction_in) {
	    	data.direction_in = data.direction_out
	    }

	    // We're using a 'poperties' object to setup whats going to be transitioned to
	    // rather than doing it all inline in the velocity function.
	    var properties = {};

	    // Since we allow the option of selecting the direction of the slide, we
	    // need to handle those how they need to be handled because they need to be
	    // handled differently if they're going to behave differently.
		if (data.direction_out == 'up') {
			properties['translateY'] = '-' + this.distances.out + 'px';
	    } else if (data.direction_out == 'left') {
	        properties['translateX'] = this.distances.out + 'px';
	    } else if (data.direction_out == 'right') {
	    	properties['translateX'] = '-' + this.distances.out + 'px';
	    } else if (data.direction_out == 'down') {
	        properties['translateY'] = this.distances.out + 'px';
	    }

	    properties['opacity'] = '0';

	    $(data.target).velocity(
	        properties,
	        {
	            duration: data.duration,
	            easing: that.easing.standard,
	            begin: function() {
	            	// Make it so the element we're moving out doesn't slide over
	            	// anything we don't want it to.
	            	// Also add the class 'transitioning' so that other scripts can
	            	// know if a transition is already underway.
	                $(data.target).css({'z-index' : '-1'}).addClass('transitioning')
	            },
	            complete: function() {

	            	// After the transition is done, we may want to move it further
	            	// so that the transition in has more (or less) distance to move.
	                if (data.direction_in == 'up') {
	                	$(data.target).velocity({'translateY' : '-' + that.distances.in + 'px'},{'duration' : 0})
				    } else if (data.direction_in == 'left') {
				    	$(data.target).velocity({'translateX' : that.distances.in + 'px'},{'duration' : 0})
				    } else if (data.direction_in == 'right') {
				    	$(data.target).velocity({'translateX' : '-' + that.distances.in + 'px'},{'duration' : 0})
				    } else if (data.direction_in == 'down') {
				    	$(data.target).velocity({'translateY' : that.distances.in + 'px'},{'duration' : 0})
				    }

	                // This callback should be used to change the view in the
	                // element that we are transitioning and to then transition
	                // the element back into view.
	                callback();
	            }
	        }
	    )
	},

	slide_in: function(data) {
	    var that = this;

	    // We want to be able to set a speed when we call the function, but we also want
	    // to have a default that we fall back to if it's not specificed.
	    if (!data.duration && data.duration !== 0) {
	    	data.duration = 350
	    }

	    // We're using a 'poperties' object to setup whats going to be transitioned to
	    // rather than doing it all inline in the velocity function.
	    var properties = {};

	    properties['translateX'] = '0px';
	    properties['translateY'] = '0px';
	    properties['opacity'] = '1';

	    $(data.target).velocity(
	        properties,
	        {
	            duration: data.duration,
	            easing: that.easing.standard,
	            complete: function() {
	            	// Reset the z-index and remove the transitioning class, since
	            	// in theory we should be done by now.
	                $(data.target).css({'z-index' : ''}).removeClass('transitioning')
	            }
	        }
	    )

	}
}

// The reason this check is here is so that the file will work nicely in both
// the Framework and as part of an App/in our build process.
if (typeof module !== 'undefined') {
	module.exports = transitions	
}