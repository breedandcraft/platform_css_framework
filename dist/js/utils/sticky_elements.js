/*
Add the class 'sticky' to an element.

If you want it to have a max scroll sticky-ness, then give the element the attribute
'data-floor="ID_OF_ELEMENT"', where 'ID_OF_ELEMENT' is the element where the
sticky-ness will be stopped at the bottom of.

If the element will also have a daterangepicker that we want to scroll along with it
then add 'data-daterangepicker="true"' to the element.

To Do:
Instead of having it be the bottom of a 'floor' element, have the option for the
floor to be the top of an element.

Maybe also have a scroll limit as the 'floor': be able to specify it to be sticky
for a maximum of 400px (or whatever value is desired)
*/

var sticky_elements = {

	// May have to store this as a 'global' variable in Application that gets
	// updated when the page changes.
	get_sticky_elements: function() {
		var elements = []

		$('.sticky').each(function() {

			// The reason why we pre-calc the values here instead of doing it on the fly
			// is that these values will change as we make elements sticky with the
			// scroll, thus it would break/not work correctly if we didn't pre-calc.

			// Make an object with the position from the top of the document and height
			// of the element.
			var element = {
				top : $(this).offset().top,
				height : $(this).height()
			}

			// Check if the sticky element has a floor (where it will stop scrolling)
			// specified.
			var floor_element = $(this).attr('data-floor')

			// If we have a value, get the position of the bottom of that element by
			// getting the top position and adding the height of the element.
			if (floor_element) {
				element.floor = $("#" + floor_element).offset().top + $("#" + floor_element).outerHeight(true)
			} else {
				// But if we don't have a value, we still want it to be something, so
				// we'll make it false so we can use it in an if statement.
				element.floor = false
			}

			if ($(this).attr('data-daterangepicker')) {
				element.daterangepicker = true
			} else {
				element.daterangepicker = false
			}

			// Add it to the array...
			elements.push(element)

		})

		return elements
	},

	scroll_daterangepicker: function(scrolled_distance) {
		// This is a bit hacky, but we're going to have date picker inputs be sticky
		// with the scroll, thus we will want to have the daterangepicker container
		// to be sticky too.
		// Not even sure if I like the input being sticky.
		var daterangepicker_element = $('.daterangepicker')
		var drp_top = parseInt(daterangepicker_element.css('top'), 10)
		var drp_scroll_start = parseInt(daterangepicker_element.attr('data-start_scroll'), 10)

		daterangepicker_element.css({
			'transform' : 'translateY(' + (0 + (scrolled_distance - drp_scroll_start)) + 'px)'
		})
	},

	// Ideally this will be called in a 'global'/Application level window scroll event.
	scroll_sticky_elements: function(elements, scrolled, header_height) {
		var that = this;
		// We're going to be looping over elements and using data from an array, so
		// we need to keep track of our loops to align them.
		var i = 0;
		$('.sticky').each(function() {
			
			// Just to make it easier to understand, assign the array object as a
			// variable.
			var element = elements[i];

			// If the element has a floor (a limit to how far it will be sticky)
			// then check to see if we're at or past that point: if true then set
			// the correct transform so that it sits at the bottom of the floor.
			if (element.floor && (element.height + scrolled + header_height) >= element.floor) {
				$(this).css({
					'transform' : 'translateY(' + (element.floor - element.top - element.height) + 'px)',
					'z-index' : 1
				}).removeClass('stickied')
			} else if (element.top <= (scrolled + header_height) && scrolled !== 0) {
				// If we're here then we've scrolled past the element and need to
				// set a transform to make it 'sticky'.
				$(this).css({
					'transform' : 'translateY(' + ((scrolled + header_height) - element.top) + 'px)',
					'z-index' : 2
				}).addClass('stickied')

				if (element.daterangepicker) {
					that.scroll_daterangepicker(scrolled);
				}
			} else {
				// If we're here then we haven't scrolled past the element, so let's
				// not apply any transform to it.
				$(this).css({
					'transform' : 'translateY(0)',
					'z-index' : 1
				}).removeClass('stickied')

				if (element.daterangepicker) {
					that.scroll_daterangepicker(scrolled);
				}
			}

			// Increment the loop counter to keep things in sync...
			i++;
		});
	}
}

// The reason this check is here is so that the file will work nicely in both
// the Framework and as part of an App/in our build process.
if (typeof module !== 'undefined') {
	module.exports = sticky_elements	
}