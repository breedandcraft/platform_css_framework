var utils = {
	test_function: function() {
		console.log('This is the test function, if you see me, everything is OK')
	}
}

// The reason this check is here is so that the file will work nicely in both
// the Framework and as part of an App/in our build process.
if (typeof module !== 'undefined') {
	module.exports = utils
}