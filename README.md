This is very much a work in progress: the intention is to have a set of UI (CSS/HTML/JS) components that we will use and share across Platform projects.

### Requirements ###

* Stylus: npm install stylus -g
* autoprefixer-stylus: npm install autoprefixer-stylus -g

### How to get going... ###

CD to the repository and run:

```
#!command line

npm install; stylus -u autoprefixer-stylus --watch src/style.styl --out dist/style.css

```

### To Do ###

* https://breedandcraft.atlassian.net/projects/PUF