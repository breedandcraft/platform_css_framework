/*
This script is used to move the font-awesome font files and jQuery to our 'dist'
folder when the package is installed.

It will run on the 'postinstall' event.
*/
var ncp = require('ncp').ncp;

// How many async copies we want to happen, 16 seems to be the default I see in
// most examples.
ncp.limit = 16;

// Copy font-awesome font files
console.log("Copying font-awesome font files to \'dist\' folder");

var source = 'node_modules/font-awesome-stylus/fonts/';
var target = 'dist/fonts/font-awesome';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("font-awesome: Done");
})

// Copy jQuery
console.log("Copying jQuery to \'dist\' folder");

var source = 'node_modules/jquery/dist/jquery.js';
var target = 'dist/js/jquery.js';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("jQuery: Done");
})

// Copy moment.js
console.log("Copying moment.js to \'dist\' folder");

var source = 'node_modules/bootstrap-daterangepicker/moment.js';
var target = 'dist/js/moment.js';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("moment.js: Done");
})

// Copy daterangepicker.js
console.log("Copying daterangepicker.js to \'dist\' folder");

var source = 'node_modules/bootstrap-daterangepicker/daterangepicker.js';
var target = 'dist/js/daterangepicker.js';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("daterangepicker.js: Done");
})

// Copy daterangepicker CSS
console.log("Copying daterangepicker CSS to \'dist\' folder");

var source = 'node_modules/bootstrap-daterangepicker/daterangepicker.css';
var target = 'dist/css/daterangepicker.css';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("daterangepicker CSS: Done");
})

// Copy c3.js
console.log("Copying c3.js to \'dist\' folder");

var source = 'node_modules/c3/c3.js';
var target = 'dist/js/c3.js';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("c3.js: Done");
})

// Copy c3.css
console.log("Copying c3.css to \'dist\' folder");

var source = 'node_modules/c3/c3.css';
var target = 'dist/css/c3.css';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("c3.css: Done");
})

// Copy d3.js
console.log("Copying d3.js to \'dist\' folder");

var source = 'node_modules/c3/node_modules/d3/d3.js';
var target = 'dist/js/d3.js';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("d3.js: Done");
})

// Copy velocity.js
console.log("Copying velocity.js to \'dist\' folder");

var source = 'node_modules/velocity-animate/velocity.js';
var target = 'dist/js/velocity.js';

ncp(source, target, function(err) {
	if (err) {
		return console.error(err);
	}
	console.log("velocity.js: Done");
})